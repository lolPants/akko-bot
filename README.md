# Weeb Bot ![](https://gitlab.com/lolPants/akko-bot/badges/master/build.svg)
_Weeb Bot for Discord_  

## Usage
### Prerequisites
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/install/)

### Docker Registry
Since the repo is private, the registry is also private. Ensure you're logged in beforehand.  
Login with `docker login registry.gitlab.com`. Use your GitLab login details to authenticate.
If you use 2FA you need to use a [personal access token](https://gitlab.com/help/user/profile/account/two_factor_authentication#personal-access-tokens) instead of a password.

More info is available [here](https://gitlab.com/lolPants/akko-bot/container_registry).

### `.env` File
Liara supports environment variables as config, so copy `example.env` to `bot.env` and fill in the details.

### Development
Start the service using `docker-compose up`  
This will start a Redis server and the Liara instance. It starts running in the foreground. `CTRL+C` to stop. You can also run it in detatched mode with `docker-compose up -d`.  
When you're done, `docker-compose rm` to remove the leftover containers.

Anything in the `weeb` folder gets linked into the `cogs/weeb` folder.  
Use `pip/requirements.txt` to add custom python modules. These get installed with a new version of the container and require it to be restarted.

### Production
Start the service in detached mode with autoreload with `docker-compose -f docker-compose.yml -f production.yml up -d`

## Maintainers
- lolPants (https://www.jackbaron.com)
