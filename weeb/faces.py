import io
import aiohttp
import discord
import asyncio
from discord.ext import commands
from cogs.utils import checks


class Faces:
    """Smug, Pouty or Confused Faces"""

    def __init__(self, liara):
        self.liara = liara
        self.help_set = {
            'group': 'Image',
            'image': 'https://safe.n3s.co/COuIwScw.jpg'
        }

    async def fetch_image(self, endpoint: str):
        async with aiohttp.ClientSession() as session:
            async with session.get(
                    'https://weeb.jackbaron.com/api/v1.0/{}/random'.format(
                        endpoint)) as resp:
                if resp.status != 200:
                    return None
                data = await resp.json()

                async with session.get(data['url']) as resp:
                    if resp.status != 200:
                        return None
                    image = io.BytesIO(await resp.read())
                    return {
                        'ext': data['ext'],
                        'index': data['index'],
                        'image': image
                    }

    async def fetch_files(self, endpoint: str, count: int):
        if count > 5: count = 5

        images = await asyncio.gather(
            *[self.fetch_image(endpoint) for i in range(count)])

        files = [
            discord.File(image['image'], '{}_{}{}'.format(
                endpoint, image['index'], image['ext'])) for image in images
        ]
        return files

    @commands.command(pass_content=True, aliases=['pouts', 'pouty'])
    @commands.cooldown(1, 2, commands.BucketType.user)
    async def pout(self, ctx, count: int = 1):
        """Get a random pouting anime girl.

        Pass an integer to `<count>` to fetch more than one!"""

        # Typing Indicator
        await ctx.trigger_typing()

        # Send Files
        files = await self.fetch_files('pout', count)
        await ctx.send(files=files)

    @commands.command(pass_content=True)
    @commands.cooldown(1, 2, commands.BucketType.user)
    async def smug(self, ctx, count: int = 1):
        """Get a random smug anime girl.

        Pass an integer to `<count>` to fetch more than one!"""

        # Typing Indicator
        await ctx.trigger_typing()

        # Send Files
        files = await self.fetch_files('smug', count)
        await ctx.send(files=files)

    @commands.command(pass_content=True, aliases=['huh'])
    @commands.cooldown(1, 2, commands.BucketType.user)
    async def confused(self, ctx, count: int = 1):
        """Get a random confused anime girl.

        Pass an integer to `<count>` to fetch more than one!"""

        # Typing Indicator
        await ctx.trigger_typing()

        # Send Files
        files = await self.fetch_files('confused', count)
        await ctx.send(files=files)

    @commands.command(pass_content=True, aliases=['laughing'])
    @commands.cooldown(1, 2, commands.BucketType.user)
    async def laugh(self, ctx, count: int = 1):
        """Get a random laughing anime girl.

        Pass an integer to `<count>` to fetch more than one!"""

        # Typing Indicator
        await ctx.trigger_typing()

        # Send Files
        files = await self.fetch_files('laugh', count)
        await ctx.send(files=files)

    @commands.command(pass_content=True)
    @commands.cooldown(1, 2, commands.BucketType.user)
    async def nagatoro(self, ctx, count: int = 1):
        """Get a random image of Nagatoro.

        Pass an integer to `<count>` to fetch more than one!"""

        # Typing Indicator
        await ctx.trigger_typing()

        # Send Files
        files = await self.fetch_files('nagatoro', count)
        await ctx.send(files=files)

    @commands.command(pass_content=True, aliases=['oldspice'], hidden=True)
    @commands.cooldown(1, 2, commands.BucketType.user)
    async def terry(self, ctx, count: int = 1):
        """Get a random image of Terry Crews.

        Pass an integer to `<count>` to fetch more than one!"""

        # Typing Indicator
        await ctx.trigger_typing()

        # Send Files
        files = await self.fetch_files('terry', count)
        await ctx.send(files=files)


def setup(liara):
    liara.add_cog(Faces(liara))
