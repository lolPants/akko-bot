import asyncio
import discord
from discord.ext import commands


class Invite:
    """Bring Weeb Bot to your own server!"""

    def __init__(self, liara):
        self.liara = liara
        self.help_set = {
            'group': 'General',
            'image': 'https://i.imgur.com/ap2Ps0d.png'
        }

    async def temp_message(self, ctx, text, timeout: int = 5):
        message = await ctx.send(text)
        await asyncio.sleep(timeout)
        try:
            await ctx.message.delete()
        except:
            pass

        try:
            await message.delete()
        except:
            pass

        return ctx

    @commands.command()
    async def invite(self, ctx):
        """Invite Weeb Bot to your server!"""

        perms = discord.Permissions()
        perms.update(
            change_nickname=True,
            view_channel=True,
            send_messages=True,
            manage_messages=True,
            embed_links=True,
            read_message_history=True,
            add_reactions=True)
        invite_str = '<https://discordapp.com/oauth2/authorize?client_id={}&permissions={}&scope=bot>'.format(
            self.liara.user.id, perms.value)

        try:
            await ctx.message.delete()
        except:
            pass

        try:
            await ctx.author.send(invite_str)
            await self.temp_message(
                ctx,
                '<:check:419950869497380865> Invite URL sent! (Check your DMs)')
        except discord.Forbidden:
            await self.temp_message(ctx, invite_str, 30)


def setup(liara):
    liara.add_cog(Invite(liara))
