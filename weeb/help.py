import discord
import asyncio
import inspect
import textwrap
from discord.ext import commands
from cogs.weeb.utils import help as h
from cogs.utils import checks

# URL to an image for the embed's logo
embed_logo = 'https://i.imgur.com/faOSTbN.jpg'


class help:
    """Custom help messages WITH FANCY EMBEDS OOOOOO!"""

    def __init__(self, liara):
        self.liara = liara
        self.help_set = {
            'group': 'General',
            'image': 'https://i.imgur.com/AZWeMcH.png'
        }

    def field_formatter(self, string: str):
        """Formats an embed field so it's smol."""
        return textwrap.fill(string, 25)

    async def help_groups(self, ctx):
        """Returns all different help groups and their commands."""
        groups = {'General': []}
        for command in self.liara.commands:
            cog = command.cog_name
            if cog == 'Core':
                if not groups.get('Core'):
                    groups['Core'] = [command]
                else:
                    groups['Core'].append(command)
            elif cog:
                cog = self.liara.get_cog(cog)
                try:
                    settings = cog.help_set
                    if settings.get('group'):
                        if not groups.get(settings['group']):
                            groups[settings['group']] = [command]
                        else:
                            groups[settings['group']].append(command)
                    else:
                        groups['General'].append(command)
                except AttributeError:
                    groups['General'].append(command)
            else:
                groups['General'].append(command)
        return groups

    async def help_formatter(self, ctx):
        groups = await self.help_groups(ctx)
        embeds = []
        # Building the top embed
        emb = discord.Embed(
            description=
            "I'm Weeb Bot, made by lolPants#0001 for all! Look below for a list of all commands you can use."
        )
        prefix = self.liara.command_prefix[0]
        emb.add_field(
            name='Made possible using',
            value=
            '[discord.py](https://github.com/Rapptz/discord.py/tree/rewrite)\n'
            '[Liara by Pandentia](https://github.com/Thessia/Liara)')
        emb.set_author(name='❓ Bot Help')
        emb.set_thumbnail(url=embed_logo)
        embeds.append(emb)
        fields = 0

        for group in groups:
            cmd_group = sorted(groups[group], key=lambda x: x.name)
            emb = discord.Embed()
            emb.set_author(name='❓ {} Commands'.format(group))
            fields = 0
            for command in cmd_group:
                can_exec = True

                for function in command.checks:
                    if function.__module__ != 'cogs.weeb.utils.p':
                        try:
                            if not function(ctx):
                                can_exec = False
                        except:
                            if not await function(ctx):
                                can_exec = False
                if not can_exec:
                    continue

                if fields == 24:
                    embeds.append(emb)
                    emb = discord.Embed()
                    emb.set_author(name='❓ {} Commands (Cont.)'.format(group))
                    fields = 0
                info = self.field_formatter(
                    command.short_doc) if command.short_doc else "ᅟᅟᅟᅟᅟᅟᅟᅟ"
                emb.add_field(
                    name=("`{}{}`").format(prefix, command.name), value=info)
                fields += 1

            if (emb not in embeds and emb.fields):
                if fields % 3 == 2:
                    emb.add_field(name="ᅟᅟᅟᅟᅟᅟᅟᅟ", value="ᅟᅟᅟᅟᅟᅟᅟᅟ")

                embeds.append(emb)
        embeds[-1].set_footer(
            text="Do !help <command> for more info on a command.")

        # Let's give the embeds some colour
        if not isinstance(ctx.channel, discord.abc.PrivateChannel):
            for emb in embeds:
                emb.colour = ctx.guild.me.colour

        # Returns all the embeds
        return embeds

    @commands.command()
    async def help(self, ctx, *, command=None):
        """Returns a list of all commands you can run, or help on a specific command."""
        if not command:
            try:
                await ctx.message.delete()
            except:
                pass
            embeds = await self.help_formatter(ctx)
            for emb in embeds:
                try:
                    await ctx.author.send(embed=emb)
                except:
                    message = await ctx.send(
                        "{} You don't have your DMs open!".format(
                            ctx.author.mention))
                    asyncio.sleep(5)
                    await message.delete()
                    return
        else:
            command = self.liara.get_command(command)
            if (command and await command.can_run(ctx)):
                emb = await h.commandEmbed(self.liara, command)
                if not isinstance(ctx.channel, discord.abc.PrivateChannel):
                    colour = ctx.guild.me.colour
                    emb.colour = colour
                await ctx.send(embed=emb)


def setup(liara):
    liara.remove_command('help')
    liara.add_cog(help(liara))
