import io
import aiohttp
import discord
import asyncio
from discord.ext import commands
from cogs.utils import checks


class Akko:
    """Fetch Akko images!"""

    def __init__(self, liara):
        self.liara = liara
        self.help_set = {
            'group': 'Image',
            'image': 'https://i.imgur.com/1SGx7Zs.jpg'
        }

    async def fetch_image(self, endpoint: str):
        """Fetches an image from the Akko CDN"""
        async with aiohttp.ClientSession() as session:
            async with session.get(
                    'https://weeb.jackbaron.com/api/v1.0/{}/random'.format(
                        endpoint)) as resp:
                if resp.status != 200:
                    return None
                data = await resp.json()

                async with session.get(data['url']) as resp:
                    if resp.status != 200:
                        return None
                    image = io.BytesIO(await resp.read())
                    return {
                        'ext': data['ext'],
                        'index': data['index'],
                        'image': image
                    }

    async def fetch_files(self, endpoint: str, count: int):
        if count > 5: count = 5

        images = await asyncio.gather(
            *[self.fetch_image(endpoint) for i in range(count)])

        files = [
            discord.File(image['image'], '{}_{}{}'.format(
                endpoint, image['index'], image['ext'])) for image in images
        ]
        return files

    @commands.command(pass_content=True)
    @commands.cooldown(1, 2, commands.BucketType.user)
    async def akko(self, ctx, count: int = 1):
        """Get a random image of Akko!

        Pass an integer to `<count>` to fetch more than one!"""

        # Typing Indicator
        await ctx.trigger_typing()

        # Send Files
        files = await self.fetch_files('akko', count)
        await ctx.send(files=files)


def setup(liara):
    liara.add_cog(Akko(liara))
