import io
import aiohttp
import discord
import asyncio
from discord.ext import commands
from cogs.utils import checks


class NekosLife:
    """Fetch images from the nekos.life API."""

    def __init__(self, liara):
        self.liara = liara
        self.help_set = {
            'group': 'Image',
            'image': 'https://i.imgur.com/wD9rng0.png'
        }

    async def fetch_image(self, endpoint: str):
        """Fetches an image from the nekos.life v2 API"""
        async with aiohttp.ClientSession() as session:
            async with session.get('https://nekos.life/api/v2/img/{}'.format(
                    endpoint)) as resp:
                if resp.status != 200:
                    return None
                # Fetch JSON
                json = await resp.json()
                url = json['url']
                ext = url.split('.')[-1]

                # Fetch from new URL
                async with session.get(url) as resp:
                    if resp.status != 200:
                        return None
                    image = io.BytesIO(await resp.read())
                    return (image, ext)

    async def fetch_files(self, endpoint: str, images: int):
        if images > 5: images = 5

        images = await asyncio.gather(
            *[self.fetch_image(endpoint) for i in range(images)])

        files = [
            discord.File(image[0], '{}_{}.{}'.format(endpoint, i, image[1]))
            for i, image in enumerate(images)
        ]
        return files

    @commands.command(pass_content=True)
    @commands.cooldown(1, 2, commands.BucketType.user)
    async def cuddle(self, ctx, images: int = 1):
        """Get a random cuddle GIF :3

        Pass an integer to `<images>` to fetch more than one!"""

        # Typing Indicator
        await ctx.trigger_typing()

        # Send Files
        files = await self.fetch_files('cuddle', images)
        await ctx.send(files=files)

    @commands.command(pass_content=True)
    @commands.cooldown(1, 2, commands.BucketType.user)
    async def hug(self, ctx, images: int = 1):
        """Get a hug GIF :3

        Pass an integer to `<images>` to fetch more than one!"""

        # Typing Indicator
        await ctx.trigger_typing()

        # Send Files
        files = await self.fetch_files('hug', images)
        await ctx.send(files=files)

    @commands.command(pass_content=True)
    @commands.cooldown(1, 2, commands.BucketType.user)
    async def kiss(self, ctx, images: int = 1):
        """Get a kiss GIF :3

        Pass an integer to `<images>` to fetch more than one!"""

        # Typing Indicator
        await ctx.trigger_typing()

        # Send Files
        files = await self.fetch_files('kiss', images)
        await ctx.send(files=files)

    @commands.command(pass_content=True)
    @commands.cooldown(1, 2, commands.BucketType.user)
    @commands.is_nsfw()
    async def lewd(self, ctx, images: int = 1):
        """Get a random *lewd* catgirl ( ͡° ͜ʖ ͡°)

        Pass an integer to `<images>` to fetch more than one!"""

        # Typing Indicator
        await ctx.trigger_typing()

        # Send Files
        files = await self.fetch_files('lewd', images)
        await ctx.send(files=files)

    @commands.command(pass_content=True)
    @commands.cooldown(1, 2, commands.BucketType.user)
    async def neko(self, ctx, images: int = 1):
        """Get a random catgirl :3

        Pass an integer to `<images>` to fetch more than one!"""

        # Typing Indicator
        await ctx.trigger_typing()

        # Send Files
        files = await self.fetch_files('neko', images)
        await ctx.send(files=files)

    @commands.command(pass_content=True)
    @commands.cooldown(1, 2, commands.BucketType.user)
    async def pat(self, ctx, images: int = 1):
        """Get a random pat GIF :3

        Pass an integer to `<images>` to fetch more than one!"""

        # Typing Indicator
        await ctx.trigger_typing()

        # Send Files
        files = await self.fetch_files('pat', images)
        await ctx.send(files=files)


def setup(liara):
    liara.add_cog(NekosLife(liara))
